<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    Route::group([
        'middleware' => 'auth:api',
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    'prefix' => 'todo-items',
    'middleware' => 'auth:api',
], function () {
    Route::post('store', 'ToDoItemController@store');
    Route::get('/', 'ToDoItemController@index');
    Route::get('{toDoItem}', 'ToDoItemController@show');
    Route::post('{toDoItem}/done', 'ToDoItemController@done');
});