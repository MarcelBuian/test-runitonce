<?php

namespace App;

use App\Models\ToDoItem;
use App\Models\UserPersonalToken;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Token;

/**
 * @property Carbon created_at
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function createPersonalToken(bool $rememberMe = false): UserPersonalToken
    {
        $tokenResult = $this->createToken('Personal Access Token');
        /** @var Token $token */
        $token = $tokenResult->token;
        if ($rememberMe) {
            $token->expires_at = now()->addWeek();
        }
        $token->save();

        return UserPersonalToken::create($tokenResult);
    }

    public function logout()
    {
        optional($this->token())->revoke();
    }

    public function todoItems(): HasMany
    {
        return $this->hasMany(ToDoItem::class);
    }
}
