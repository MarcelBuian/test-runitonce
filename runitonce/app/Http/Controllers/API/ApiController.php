<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

abstract class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function createdResponse($message, array $data = [])
    {
        return $this->successResponse($message, $data, 201);
    }

    public function successResponse($message, array $data = [], $statusCode = 200)
    {
        return response()->json(compact('message', 'data'), $statusCode);
    }

    public function getLoggedUser(): User
    {
        return auth()->user();
    }
}
