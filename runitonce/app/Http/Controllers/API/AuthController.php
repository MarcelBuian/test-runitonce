<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register()
    {
        $inputs = $this->validate(request(), [
            'name' => 'required|string|min:2|max:255',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = User::create($inputs);
        // @TODO: trigger an event: user created, maybe send an email confirmation

        return $this->createdResponse('Successfully created user!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthenticationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $inputs = $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean',
        ]);

        if(!Auth::attempt([
            'email' => $inputs['email'],
            'password' => $inputs['password'],
        ])) {
            throw new AuthenticationException('Invalid email or/and password');
        }

        /** @var User $user */
        $user = $request->user();

        $personalToken = $user->createPersonalToken($inputs['remember_me'] ?? false);

        return $this->createdResponse('Successfully created access token!', [
            'access_token' => $personalToken->getAccessToken(),
            'token_type' => $personalToken->getTokenType(),
            'expires_at' => $personalToken->getExpiresAt()->toDateTimeString(),
        ]);
    }

    public function logout()
    {
        $this->getLoggedUser()->logout();

        return $this->successResponse('Successfully logged out');
    }

    public function user()
    {
        return $this->successResponse('Showing user details', $this->getLoggedUser()->toArray());
    }
}
