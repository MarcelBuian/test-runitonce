<?php

namespace App\Http\Controllers\API;

use App\Models\ToDoItem;
use App\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ToDoItemController extends ApiController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $inputs = $this->validate(request(), [
            'title' => 'required|string|min:2|max:255',
            'description' => 'string',
        ]);
        $inputs['user_id'] = $request->user()->id;
        $inputs['done'] = false;
        $toDoItem = ToDoItem::query()->create($inputs);

        return $this->createdResponse('Successfully created todo item!', $toDoItem->toArray());
    }

    public function show(ToDoItem $toDoItem)
    {
        return $this->successResponse('Displaying todo item info', $toDoItem->toArray());
    }

    public function done(ToDoItem $toDoItem)
    {
        if ($toDoItem->done) {
            throw new HttpResponseException(response(['message' => 'Item is already done'], 403));
        }

        $toDoItem->done = true;
        $toDoItem->save();

        return $this->successResponse('ToDo item is marked as done.', $toDoItem->toArray());
    }

    public function index()
    {
        /** @var User $user */
        $user = auth()->user();
        /** @var Collection $toDoItems */
        $toDoItems = $user->todoItems;

        $message = 'Showing '.count($toDoItems).' todoitems';

        return $this->successResponse($message, $toDoItems->toArray());
    }
}
