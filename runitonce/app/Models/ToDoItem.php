<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @property int id
 * @property string title
 * @property string description
 * @property boolean done
 * @property int user_id
 * @property-read User user
 * @property Carbon created_at
 */
class ToDoItem extends Model
{
    protected $table = 'todoitems';

    public function resolveRouteBinding($value)
    {
        return $this->where($this->getRouteKeyName(), $value)
            ->where('user_id', request()->user()->id)
            ->first()
        ;
    }

    protected $fillable = [
        'title',
        'description',
        'done',
        'user_id',
    ];

    protected $casts = [
        'done' => 'boolean',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
