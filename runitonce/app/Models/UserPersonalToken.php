<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Laravel\Passport\PersonalAccessTokenResult;
use Laravel\Passport\Token;

class UserPersonalToken
{
    const TOKEN_TYPE = 'Bearer';

    /** @var PersonalAccessTokenResult */
    private $tokenResult;

    /**
     * UserPersonalToken constructor.
     *
     * @param PersonalAccessTokenResult $tokenResult
     */
    public function __construct(PersonalAccessTokenResult $tokenResult)
    {
        $this->tokenResult = $tokenResult;
    }

    /**
     * @param PersonalAccessTokenResult $tokenResult
     * @return UserPersonalToken
     */
    public static function create(PersonalAccessTokenResult $tokenResult)
    {
        return new self($tokenResult);
    }

    public function getAccessToken(): string
    {
        return $this->tokenResult->accessToken;
    }

    public function getTokenType(): string
    {
        return self::TOKEN_TYPE;
    }

    public function getExpiresAt(): Carbon
    {
        return $this->tokenResult->token->expires_at;
    }

    public function getToken(): Token
    {
        return $this->tokenResult->token;
    }
}
