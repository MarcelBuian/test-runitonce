<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    private function request(?string $accessToken)
    {
        return $this->get('/api/auth/user', $this->getRequestHeaders($accessToken));
    }

    private function assertUserResponse(User $user, TestResponse $response)
    {
        $response->assertStatus(200);
        $response->assertJsonFragment(['message' => 'Showing user details']);
        $response->assertJsonStructure(['message', 'data']);
        $response->assertJsonFragment([
            'email' => $user->email,
            'name' => $user->name,
            'id' => $user->id,
            'created_at' => $user->created_at->toDateTimeString(),
        ]);
    }

    public function testShowingUserDetailsWithAccessToken()
    {
        $user = $this->createUserWithPass();
        $accessToken = $this->createAccessTokenFor($user);
        $response = $this->request($accessToken);

        $this->assertUserResponse($user, $response);
    }

    public function testShowingUserDetailsMocked()
    {
        $this->withExceptionHandling();
        $user = $this->createUserAndLogin();
        $response = $this->request(null);

        $this->assertUserResponse($user, $response);
    }

    public function testMissingAccessToken()
    {
        $user = $this->createUserWithPass();
        $accessToken = $this->createAccessTokenFor($user);
        $response = $this->request(null);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    public function testWrongAccessToken()
    {
        $user = $this->createUserWithPass();
        $accessToken = $this->createAccessTokenFor($user);
        $response = $this->request($accessToken.'wrong');

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }
}
