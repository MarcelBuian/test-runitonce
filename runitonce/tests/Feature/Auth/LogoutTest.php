<?php

namespace Tests\Feature\Auth;

use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    private function request(string $accessToken)
    {
        return $this->get('/api/auth/logout', $this->getRequestHeaders($accessToken));
    }

    public function testLogout()
    {
        $user = $this->createUserWithPass();
        $personalToken = $user->createPersonalToken();
        $token = $personalToken->getToken();
        $accessToken = $personalToken->getAccessToken();

        $this->assertFalse($token->revoked);
        $response = $this->request($accessToken);
        $this->assertTrue($token->refresh()->revoked);
        $response->assertStatus(200);
        $response->assertJsonFragment(['message' => 'Successfully logged out']);
    }

    public function testWrongAccessToken()
    {
        $user = $this->createUserWithPass();
        $accessToken = $this->createAccessTokenFor($user);
        $response = $this->request($accessToken.'wrong');

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    public function testRevokedToken()
    {
        $user = $this->createUserWithPass();
        $personalToken = $user->createPersonalToken();
        $token = $personalToken->getToken();
        $accessToken = $personalToken->getAccessToken();
        $token->revoke();
        $this->assertTrue($token->revoked);
        $response = $this->request($accessToken);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }
}
