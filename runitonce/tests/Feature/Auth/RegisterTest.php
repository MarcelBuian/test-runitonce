<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Support\Facades\Hash;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    protected function getInput(): array
    {
        return [
            'name' => $name = 'Marcel',
            'email' => $email = 'buianmarcel@gmail.com',
            'password' => $password = 'secret',
            'password_confirmation' => $password,
        ];
    }

    private function request(array $inputs)
    {
        return $this->post('/api/auth/register', $inputs, $this->getRequestHeaders());
    }

    public function testSuccessfulCreated()
    {
        $this->handleValidationExceptions();
        $input = $this->getInput();
        $response = $this->request($input);

        $response->assertStatus(201);
        $response->assertJsonFragment(['message' => 'Successfully created user!']);

        $user = User::query()->whereEmail($input['email'])->first();
        $this->assertNotNull($user);
        $this->assertSame($user->name, $input['name']);
        $this->assertNotSame($user->password, $input['password']);
        $this->assertTrue(Hash::check($input['password'], $user->password));
        $this->assertDatabaseHas('users', [
            'name' => $input['name'],
            'email' => $input['email'],
        ]);
    }

    public function testMissingPasswordConfirmation()
    {
        $input = $this->getInput();
        unset($input['password_confirmation']);
        $response = $this->request($input);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('password');
        $response->assertJsonFragment(["The password confirmation does not match."]);
    }

    public function testPasswordConfirmationDoesntMatch()
    {
        $input = $this->getInput();
        $input['password_confirmation'] = 'another_secret';
        $response = $this->request($input);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('password');
        $response->assertJsonFragment(["The password confirmation does not match."]);
    }

    public function testNameIsShort()
    {
        $input = $this->getInput();
        $input['name'] = 'a';
        $response = $this->request($input);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('name');
        $response->assertJsonFragment(["The name must be at least 2 characters."]);
    }

    public function testMissingEmail()
    {
        $input = $this->getInput();
        unset($input['email']);
        $response = $this->request($input);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
        $response->assertJsonFragment(["The email field is required."]);
    }

    public function testEmailExistsAlready()
    {
        $existingUser = factory(User::class)->create();
        $input = $this->getInput();
        $input['email'] = $existingUser->email;
        $response = $this->request($input);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
        $response->assertJsonFragment(["The email has already been taken."]);
    }
}
