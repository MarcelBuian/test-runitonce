<?php

namespace Tests\Feature\Auth;

use App\User;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    protected function createUser(string $password): User
    {
        return factory(User::class)->create(compact('password'));
    }

    private function request(string $email, string $password, ?bool $rememberMe = null)
    {
        $inputs = compact('email', 'password');
        if (is_bool($rememberMe)) {
            $inputs['remember_me'] = $rememberMe;
        }

        return $this->post('/api/auth/login', $inputs, $this->getRequestHeaders());
    }

    public function testSuccessfulLoggedIn()
    {
        $this->handleValidationExceptions();
        $user = $this->createUser($password = 'superSecreT');
        $response = $this->request($user->email, $password);

        $response->assertStatus(201);
        $response->assertJsonFragment(['message' => 'Successfully created access token!']);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'access_token',
                'token_type',
                'expires_at',
            ],
        ]);
    }

    public function testInvalidPassword()
    {
        $user = $this->createUser($password = 'superSecreT');
        $response = $this->request($user->email, $password.'wrong');

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Invalid email or/and password']);
    }

    public function testInvalidEmail()
    {
        $user = $this->createUser($password = 'superSecreT');
        $response = $this->request($user->email.'wrong', $password);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Invalid email or/and password']);
    }
}
