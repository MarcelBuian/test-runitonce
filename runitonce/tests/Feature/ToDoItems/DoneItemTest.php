<?php

namespace Tests\Feature\ToDoItems;

use App\Models\ToDoItem;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class ShowItemTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    private function request(int $itemId)
    {
        return $this->get('/api/todo-items/'.$itemId, $this->getRequestHeaders());
    }

    /** @group one */
    public function testItemIsDisplayed()
    {
        $this->handleValidationExceptions();
        $user = $this->createUserAndLogin();
        $todoItem = $this->createToDoItemFor($user);

        $response = $this->request($todoItem->id);

        /** @var ToDoItem $item */
        $item = ToDoItem::query()->first();
        $response->assertStatus(200);
        $response->assertJsonFragment(['message' => 'Displaying todo item info']);
        $response->assertJsonFragment([
            'id' => $item->id,
            'title' => $item->title,
            'description' => $item->description,
            'done' => $item->done,
            'created_at' => $item->created_at->toDateTimeString(),
        ]);
    }

    public function testUserMustOwnTheItem()
    {
        $user = $this->createUserAndLogin();
        $anotherUser = $this->createNewUser();
        $todoItem = $this->createToDoItemFor($anotherUser);

        $response = $this->request($todoItem->id);

        $response->assertStatus(404);
    }

    public function testInvalidId()
    {
        $user = $this->createUserAndLogin();
        $todoItem = $this->createToDoItemFor($user);

        $response = $this->request(888);

        $response->assertStatus(404);
        $response->assertJsonFragment(["No query results for model [App\\Models\\ToDoItem]."]);
    }

    public function testUserMustBeLoggedIn()
    {
        $user = $this->createNewUser();
        $todoItem = $this->createToDoItemFor($user);

        $response = $this->request($todoItem->id);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }
}
