<?php

namespace Tests\Feature\ToDoItems;

use App\Models\ToDoItem;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class DoneItemTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    private function request(int $itemId)
    {
        return $this->post("/api/todo-items/{$itemId}/done", [], $this->getRequestHeaders());
    }

    /** @group one */
    public function testItemIsDone()
    {
        $this->handleValidationExceptions();
        $user = $this->createUserAndLogin();
        $todoItem = $this->createToDoItemFor($user, ['done' => false]);
        $this->assertFalse($todoItem->done);

        $response = $this->request($todoItem->id);

        /** @var ToDoItem $item */
        $item = ToDoItem::query()->first();
        $response->assertStatus(200);
        $response->assertJsonFragment(['message' => 'ToDo item is marked as done.']);
        $response->assertJsonFragment([
            'id' => $item->id,
            'title' => $item->title,
            'description' => $item->description,
            'done' => true,
            'created_at' => $item->created_at->toDateTimeString(),
        ]);
        $this->assertTrue($todoItem->fresh()->done);
    }

    public function testUserMustOwnTheItem()
    {
        $user = $this->createUserAndLogin();
        $anotherUser = $this->createNewUser();
        $todoItem = $this->createToDoItemFor($anotherUser, ['done' => false]);

        $response = $this->request($todoItem->id);

        $response->assertStatus(404);
    }

    public function testInvalidId()
    {
        $user = $this->createUserAndLogin();
        $todoItem = $this->createToDoItemFor($user, ['done' => false]);

        $response = $this->request(888);

        $response->assertStatus(404);
        $response->assertJsonFragment(["No query results for model [App\\Models\\ToDoItem]."]);
    }

    public function testUserMustBeLoggedIn()
    {
        $this->withExceptionHandling();
        $user = $this->createNewUser();
        $todoItem = $this->createToDoItemFor($user);

        $response = $this->request($todoItem->id);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    public function testItemMustBeUndone()
    {
        $user = $this->createUserAndLogin();
        $todoItem = $this->createToDoItemFor($user, ['done' => true]);

        $response = $this->request($todoItem->id);

        $response->assertStatus(403);
        $response->assertJsonFragment(['message' => 'Item is already done']);
    }
}
