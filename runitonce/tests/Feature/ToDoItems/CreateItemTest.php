<?php

namespace Tests\Feature\ToDoItems;

use App\Models\ToDoItem;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class CreateItemTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    private function getToDoItemData(): array
    {
        return [
            'title' => 'New Itdaem',
            'description' => 'New description',
        ];
    }

    private function request(array $data)
    {
        return $this->post('/api/todo-items/store', $data, $this->getRequestHeaders());
    }

    /** @group one */
    public function testItemIsCreated()
    {
        $this->handleValidationExceptions();
        $user = $this->createUserAndLogin();
        $data = $this->getToDoItemData();
        $response = $this->request($data);

        $response->assertStatus(201);
        $response->assertJsonFragment(['message' => 'Successfully created todo item!']);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'id',
                'title',
                'description',
                'done',
                'created_at',
            ],
        ]);
        $item = ToDoItem::query()->first();
        $this->assertSame($data['title'], $item->title);
        $this->assertSame($data['description'], $item->description);
        $this->assertSame(false, $item->done);
        $this->assertSame($user->id, $item->user_id);
    }

    public function testTitleIsRequired()
    {
        $user = $this->createUserAndLogin();
        $response = $this->request([]);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('title');
        $response->assertJsonFragment(["The title field is required."]);
    }

    public function testUserMustBeLoggedIn()
    {
        $this->createNewUser();
        $input = $this->getToDoItemData();
        $response = $this->request($input);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }
}
