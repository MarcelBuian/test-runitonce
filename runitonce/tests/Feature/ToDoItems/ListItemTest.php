<?php

namespace Tests\Feature\ToDoItems;

use App\Models\ToDoItem;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class ListItemTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    private function request()
    {
        return $this->get("/api/todo-items", $this->getRequestHeaders());
    }

    /** @group one */
    public function testSuccess()
    {
        $this->handleValidationExceptions();
        $user = $this->createUserAndLogin();
        $anotherUser = $this->createNewUser();
        $todoItem1 = $this->createToDoItemFor($user);
        $todoItem2 = $this->createToDoItemFor($user);
        $todoItem3 = $this->createToDoItemFor($anotherUser);

        $response = $this->request();

        $response->assertStatus(200);
        $response->assertJsonFragment(['message' => 'Showing 2 todoitems']);
        $response->assertJsonFragment([
            'id' => $todoItem1->id,
            'title' => $todoItem1->title,
            'description' => $todoItem1->description,
            'done' => $todoItem1->done,
            'created_at' => $todoItem1->created_at->toDateTimeString(),
        ]);
        $response->assertJsonFragment([
            'id' => $todoItem2->id,
            'title' => $todoItem2->title,
            'description' => $todoItem2->description,
            'done' => $todoItem2->done,
            'created_at' => $todoItem2->created_at->toDateTimeString(),
        ]);
        $response->assertJsonCount(2, 'data');
    }

    public function testEmptyList()
    {
        $user = $this->createUserAndLogin();
        $response = $this->request();

        $response->assertJsonFragment(['message' => 'Showing 0 todoitems']);
        $response->assertJsonCount(0, 'data');
        $response->assertStatus(200);
    }

    public function testUserMustBeLoggedIn()
    {
        $response = $this->request();

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }
}
