<?php

namespace Tests;

use App\User;

trait RequestsTrait
{
    protected function createAccessTokenFor(User $user)
    {
        return $user->createPersonalToken()->getAccessToken();
    }

    /**
     * @param string|null $accessToken
     * @return array
     */
    protected function getRequestHeaders(?string $accessToken = null): array
    {
        $headers = ['Accept' => 'application/json'];
        if ($accessToken) {
            $headers['Authorization'] = 'Bearer '.$accessToken;
        }

        return $headers;
    }
}
