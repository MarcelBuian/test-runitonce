<?php

namespace Tests;

use App\Models\ToDoItem;
use App\User;
use Laravel\Passport\Passport;

trait CreateModels
{
    public function createNewUser(array $data = []): User
    {
        return factory(User::class)->create($data);
    }

    public function createToDoItemFor(User $user, array $data = []): ToDoItem
    {
        return $this->createToDoItem(array_merge(['user_id' => $user->id], $data));
    }

    public function createToDoItem(array $data = []): ToDoItem
    {
        return factory(ToDoItem::class)->create($data);
    }

    public function createUserWithPass(string $password = 'very_secret_believe_me', array $data = []): User
    {
        return $this->createNewUser(array_merge(compact('password'), $data));
    }

    public function createUserAndLogin(array $data = []): User
    {
        return tap($this->createNewUser($data), function($user) {
            Passport::actingAs($user);
        });
    }
}
