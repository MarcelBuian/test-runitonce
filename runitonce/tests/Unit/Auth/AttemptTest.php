<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Auth;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class AttemptTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    public function testSuccess()
    {
        $user = $this->createUserWithPass($password = '@!secret!@#');
        $credentials = [
            'email' => $user->email,
            'password' => $password,
        ];
        $this->assertTrue(Auth::attempt($credentials));
        $this->assertNotNull($authUser = Auth::user());
        $this->assertSame($user->email, $authUser->email);
    }

    public function testWrongPassword()
    {
        $user = $this->createUserWithPass();
        $credentials = [
            'email' => $user->email,
            'password' => 'wrong password',
        ];
        $this->assertFalse(Auth::attempt($credentials));
        $this->assertNull(Auth::user());
    }

    public function testWrongEmail()
    {
        $user = $this->createUserWithPass($password = 'great');
        $credentials = [
            'email' => $user->email.'wrong',
            'password' => $password,
        ];
        $this->assertFalse(Auth::attempt($credentials));
        $this->assertNull(Auth::user());
    }
}
