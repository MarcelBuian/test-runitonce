<?php

namespace Tests\Unit;

use App\Models\UserPersonalToken;
use Laravel\Passport\PersonalAccessTokenResult;
use Laravel\Passport\Token;
use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class UserPersonalTokenTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    public function testCreateToken()
    {
        $user = $this->createUserWithPass($password = '@!secret!@#');
        $tokenResult = $user->createToken($tokenName = 'personal');
        $this->assertSame($tokenName, $tokenResult->token->name);

        $this->assertInstanceOf(PersonalAccessTokenResult::class, $tokenResult);
    }

    public function testCreateUserToken()
    {
        $user = $this->createUserWithPass($password = 'best_poker_player_777');
        $personalToken = $user->createPersonalToken();

        $this->assertNull($user->token());

        $this->assertInstanceOf(UserPersonalToken::class, $personalToken);
        $this->assertSame('Bearer', $personalToken->getTokenType());
        $this->assertNotEmpty($accessToken = $personalToken->getAccessToken());
        $this->assertGreaterThan(now(), $personalToken->getExpiresAt());
        $this->assertInstanceOf(Token::class, $personalToken->getToken());
    }
}
