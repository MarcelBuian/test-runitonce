<?php

namespace Tests\Unit;

use Tests\RefreshDatabaseWithPassport;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use RefreshDatabaseWithPassport;

    public function testSuccess()
    {
        $user = $this->createUserWithPass();
        $token = $user->createPersonalToken()->getToken();
        $user->withAccessToken($token);

        $this->assertFalse($token->revoked);
        $user->logout();
        $this->assertTrue($token->revoked);
    }
}
