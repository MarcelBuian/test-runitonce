<?php

namespace Tests\Unit\Model;

use App\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testPasswordIsHashed()
    {
        $user = factory(User::class)->make(['password' => $password = 'secret!@#']);
        $this->assertNotSame($user->password, $password);
        $this->assertTrue(Hash::check($password, $user->password));
        $this->assertFalse(Hash::check($password, 'wrong_pass'));
    }
}
