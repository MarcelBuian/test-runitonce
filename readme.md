## Install and run
- Download and install docker
    - Ensure you have shared proper driver
- Add the domains to the hosts files.
    - `127.0.0.1  runitonce.test`
- Prepare project .env file
    - `cd runitonce`
    - `cp .env.example .env`
    - Take a look on this file if you need to do any other changes. Defaults should be ok. Don't touch mysql values
    - `cd ..`
- Goto laradock folder
    - `cd laradock`
- Prepare docker .env file
    - `cp env-example .env`
    - Take a look on this file if you need to do any other changes. Defaults should be ok.
- Build the environment and run it using docker-compose:
    - `docker-compose up -d nginx mysql phpmyadmin`
- Enter the MySQL container to create databases
    - `docker-compose exec mysql bash`
    - `mysql -uroot -psecret`
    - `create database runitonce`
    - `create database testing`
    - `exit`
    - `exit`
- Enter the Workspace container
    - `docker-compose exec workspace bash`
    - Run composer
        - `composer install`
    - Run installation key `php artisan key:generate --ansi`
    - Run migrations and passport install
        - `art migrate`
        - `art passport:install`
    - `exit`
- Open your browser and visit
    - `http://runitonce.test:8087/`
    
## To make a change in docker configs
- For simple changes
    - `docker-compose down`
    - `docker-compose up -d nginx mysql phpmyadmin`
- To rebuild image
    - `docker-compose build --no-cache nginx` or
    - `docker-compose build --no-cache mysql`

## Check database:
- External: Database is using different port to avoid conflicts with potentially local mysql installed.
    - hostname: `127.0.0.1`
    - user: `root`
    - port: `root`
    - password: `secret`
    - local database: `runitonce`
    - test database: `testing`
- PHP My Admin:
    - Goto: `http://localhost:8086`
    - Server: `mysql`
    - Username: `root`
    - Password: `secret`

## Run tests
- Goto laradock folder
    - `cd laradock`
    - Enter the Workspace container
        - `docker-compose exec workspace bash`
    - Run tests
        - `vendor/bin/phpunit`
        - `exit`

## API routes:
    - auth:
        POST /api/auth/register
        POST /api/auth/login
        GET /api/auth/logout
        GET /api/auth/user
    - todoitems:
        GET /api/todo-items
        POST /api/todo-items/store
        GET /api/todo-items/{id}
        POST /api/todo-items/{id}/done
        
